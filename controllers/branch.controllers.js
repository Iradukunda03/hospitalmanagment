const express = require('express')
const mongoose = require('mongoose')
const admin= require('../middleware/admin')
const auth= require('../middleware/auth')
const Branch = mongoose.model('Branch')

const validate = require('../model/branch.model')

const router = express.Router()

// INSERT NEW BRANCH
router.post('/', (req, res) => {
    const {error} = validate(req.body)
    if (error) return res.send(error.details[0].message).status(400)
    insertIntoDb(req, res);
})

//UPDATE AN BRANCH
// router.put('/update', [{auth,admin}],async(req, res)=>{
//     const {error} = validate(req.body)
//     if (error) return res.send(error.details[0].message).status(400)
//     updateIntoDb(req, res);
// })

//GET ALL BRANCH
router.get('/', (req, res) => {
   Branch.find()
     .then(branch=>res.send(branch))
     .catch(err=>res.send(err))
})

//GET IT BY ID
router.get('/:id', (req, res) => {
    Branch.findById(req.params.id)
        .then(BrnchSaved => res.send(BrnchSaved))
        .catch(err => res.send(err))
})
//REMOVE BRANCH
// router.delete('/:id', [{auth,admin}],(req, res) => {
//     Branch.findByIdAndRemove(req.params.id)
//         .then(brnchSav => res.send(brnchSav))
//         .catch(err => res.send(err))
// })

//FUNCTION TO INSERT 
function insertIntoDb(req, res) {
    var branch= new Branch();
    branch.branchName = req.body.branchName;
    branch.branchHead = req.body.branchHead;
    branch.branchDescription = req.body.branchDescription;
    branch.save()
        .then( brnchSave=> res.send(brnchSave).status(201))
        .catch(err => res.send(err).status(400))
}


//FUNCTION TO UPDATE
function updateIntoDb(req, res) {
    Branch.findByIdAndUpdate( { _id: req.body._id }, req.body , {new:true})
        .then(brnch => res.send(brnch).status(201))
        .catch(err => res.send(err))
}

module.exports = router