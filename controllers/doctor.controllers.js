//GET ALL DOCTORS IN A PARTICULAR BRANCH 
//GET TOTAL NUMBER OF DOCTORS
//GET TOTAL BRANCH 

const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const Branch = mongoose.model('Branch')
const Doctor= mongoose.model('Doctor')
const validate = require('../model/doctor.model')
const admin = require('../middleware/admin')
const auth = require('../middleware/auth')

let docs = new Doctor()

router.post('/',(req, res) => {
    const {error} = validate(req.body)
    if (error) return res.send(error.details[0].message).status(400)
    insertIntoDb(req, res)
})


router.get('/',async(req,res)=>{
    const docs= await Doctor.find();
    return res.send(docs);
})
//GET BY ID
router.get('/:id',(req,res)=>{
    Doctor.findById({_id:req.params.id})
        .then(cat=>res.send(cat))
        .catch(err=>send(err).status(404))
})

router.put('/update',[auth,admin],async(req,res)=>{
    const {error} = validate(req.body)
    if (error) return res.send(error.details[0].message).status(400)
    updateIntoDb(req, res);
})

router.delete('/:id',admin, (req, res) => {
    Doctor.findByIdAndRemove(req.params.id)
        .then(f => res.send(f))
        .catch(err => res.send(err).status(404));
})

function updateIntoDb(req,res){
    Doctor.findOneAndUpdate({_id:req.body._id},
        req.body,{new:true})
          .then(doc=>res.send(doc))
          .catch(err=>res.send(err).status(400));
}

//INSERT FUNCTION
function insertIntoDb(req, res) {
    var doc = new Doctor;
    doc.name = req.body.name,
    doc.email = req.body.email,
    doc.gender = req.body.gender,
    doc.branchId= req.body.branchId

    doc.save()
        .then(docSaved => res.send(docSaved))
        .catch(err => res.send(err))
}

module.exports = router