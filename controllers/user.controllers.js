const hashPassword = require('../utils/hash')
const express = require('express')
const _ = require('lodash')
const admin= require('../middleware/admin')
const auth = require('../middleware/auth')
const mongoose = require('mongoose')
const validate = require('../model/user.model')
const User = mongoose.model('User')
const router = express.Router()


// USERS INSERT
router.post('/', async(req, res) => {
    const {error} = validate(req.body)
    
    if (error) return res.send(error.details[0].message).status(400)

    let user = await User.findOne({ email: req.body.email })
   
    if (user) return res.send("user already registered").status(400)

    user = new User(_.pick(req.body, ['name', 'email', 'password', 'isAdmin']))

    const hashed = await hashPassword(user.password)
   
    user.password = hashed
    await user.save()
    // user = new User(_.pick(req.body, ['name', 'email', 'password', 'isAdmin']))
    // let user = new User(_.pick(req.body, ['name', 'email', 'password']))
    // const hashed = hash(user.password)
    // user.password = hashed
    // user.save()

    return res.send(_.pick(req.body, ['_id', 'name', 'email', 'isAdmin'])).status(201)
})

router.get('/', async (req,res) => {
    const users = await User.find();
    return res.send(users);
})


//USER RETRIEVE BY EMAIL
router.get('/:email',async (req,res)=>{
    const users = await User.find({email: req.params.email});
    return res.send(users)
});

// NON-ADMIN
router.get('/ge/non-admin',[auth,admin],async (req,res)=>{
    const users = await User.find({isAdmin:false})
    return res.send(users)
});

//USER UPDATE
// router.put('/', admin, async(req, res) => {
//     const {error} = validate(req.body)
//     if (error) return res.send(error.details[0].message).status(400)

//     let user = await User.findOne({ email: req.body.email })
//     let notAdmin= user.find({isAdmin:false})
//     if (notAdmin) return res.send("user already registered").status(400)
//     updateIntoDb(req, res)

// })


// ADMIN 
router.get('/sa/admin/',async(req,res)=>{
    const users = await User.find({isAdmin:true})
    return res.send(users)
});

//DELETE USER
router.delete('/remove/:id',[auth,admin],async (req,res)=>{
    const users = await User.findByIdAndRemove(req.params.id);
    return res.send(users)
});

module.exports=router