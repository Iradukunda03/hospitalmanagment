// const _ = require('lodash')
const bcrypt = require('bcrypt')
const mongoose = require("mongoose")
const Joi = require('joi')
const User = mongoose.model('User')
const express = require('express')

var router = express.Router()

router.post('/jwt', async(req, res) => {
    const { error } = validate(req.body)
    if (error) return res.send(error.details[0].message).status(400)

    let user = await User.findOne({ email: req.body.email })

    if (!user) return res.send("Invalid email or password").status(400)

    const validPassword = await bcrypt.compare(req.body.password, user.password)
    if (!validPassword) return res.send("Invalid password").status(400)

    return res.send(user.generateAuthToken())

})

//VALIDATION  WITH JOI FOR USER 
function validate(user) {
    const schema = {
        email: Joi.string().min(3).max(50).required(),
        password: Joi.string().min(2).max(90).required(),
        isAdmin:Joi.boolean().required()
    }

    return Joi.validate(user, schema)
}

module.exports = router