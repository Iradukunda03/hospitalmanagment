const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

var branchModel = new mongoose.Schema({
    branchName: {
        type: String,
        required: 'This field is required'
    },
    branchHead: {
        type: String,
        minlength: 2,
        maxlength: 255,
        required: true
    },
    branchDescription: {
        type: String,
        minlength: 2,
        maxlength: 255,
        required: true
    }

});

function validateBranch(branch) {
    let schema = {
        branchName: Joi.string().min(2).max(255).required(),
        branchHead: Joi.string().min(2).max(255).required(),
        branchDescription: Joi.string().min(2).max(255).required()
    }
    return Joi.validate(branch, schema)

}
mongoose.model('Branch', branchModel );
module.exports = validateBranch