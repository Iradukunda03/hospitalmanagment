const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/HospitalManagement', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('connected to mongodb successfully....'))
    .catch(err => console.log('failed to connect to mongodb', err));

    require('./user.model');
    require('./branch.model');
    require('./doctor.model');