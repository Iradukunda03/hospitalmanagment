
const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)


//Attributes of the product object

var doctorSchema = new mongoose.Schema({

    name: {
        type: String,
        minlength: 2,
        maxlength: 255,
        required: 'This field is required'
    },
    email: {
        type:String,
        minlength: 5,
        maxlength: 255
    },
    gender: {
        type: String,
        minlength: 2,
        maxlength: 50
    },
    branchId:{
        type:String,
        minlength:2,
        maxlength:255,
    }

});



mongoose.model('Doctor', doctorSchema);

function validateDoctor(doctor) {
    let schema = {
        name:Joi.string().min(2).max(255).required(),
        email:Joi.string().min(3).max(255).required(),
        branchId: Joi.objectId().required(),
        gender: Joi.string().min(2).max(50)
    }
   
    return Joi.validate(doctor, schema)
}

module.exports = validateDoctor
