// const mongoose = require('mongoose');
// const Joi = require('joi')
// const bcrypt = require('bcrypt')
// const jwt  =  require('jsonwebtoken')
// const config = require('config')
// const User = mongoose.model('user', userSchema);

// //VALIDATION NEXT TO THE MONGODB
// var userSchema = new mongoose.Schema({
//     name: {
//         type: String,
//         required: true,
//         maxlength: 255,
//         minlength: 3
//     },
//     email: {
//         type: String,
//         required: true,
//         unique: true,
//         maxlength: 255,
//         minlength: 3
//     },
//     password: {
//         type: String,
//         required: true,
//         maxlength: 255,
//         minlength: 3
//    },
//     isAdmin:{
//         type:Boolean,
//         required:true
//     }
// });
// //A METHOD TO GENERATE USER TOKEN
// userSchema.methods.generateAuthToken = function(){
//     const token  =jwt.sign({_id:this._id,name:this.name,isAdmin:this.isAdmin,email:this.email}
//         ,config.get('jwtPrivateKey'))
//     return token
// }


//  module.exports=function validateUser(user){
//     const schema = {
//         name:Joi.string().max(255).min(3).required(),
//         email: Joi.string().max(255).min(3).required().email(),
//         password:Joi.string().max(255).min(3).required(),
//         isAdmin:Joi.required()
//     }
//     return Joi.validate(user,schema)
// }
// module.exports.User = User

// module.exports.validate= validateUser



const mongoose = require('mongoose')
const config = require('config')
const bcrypt = require('bcrypt')
const Joi = require('joi')
const jwt = require('jsonwebtoken')


var userSchema = new mongoose.Schema({
    name: {
        type: String(),
    },
    email: {
        type: String,

    },
    password: {
        type: String,

    },
    isAdmin: {
        type: Boolean,
        required:true
    }

})
userSchema.methods.generateAuthToken = function() {
    const token = jwt.sign({ email: this.email,isAdmin:this.isAdmin}, config.get('jwt_privateKey'))
    return token
}

mongoose.model('User', userSchema)

function validateUser(user) {
    const Schema = {
        name: Joi.string().min(3).max(255).required(),
        email: Joi.string().min(3).max(50).required(),
        password: Joi.string().min(2).max(90).required(),
        isAdmin: Joi.boolean().required()

    }
    return Joi.validate(user, Schema)
}
module.exports = validateUser