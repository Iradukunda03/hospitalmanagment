var express = require('express')
var mongodb = require('./model/mongoDb')
var doctorController = require('./controllers/doctor.controllers')
var branchController = require('./controllers/branch.controllers')
var authController = require('./controllers/auth.controllers')
var authMiddleWare = require('./middleware/auth')
var controllers = require('./controllers/user.controllers')
const config = require('config')


var app = express()
const bodyparser = require('body-parser')

app.use(bodyparser.urlencoded({ extended: true }))
app.use(bodyparser.json())
if (!config.get("jwt_privateKey")) {
    console.log('JWT PRIVATE KEY IS NOT DEFINED')
    process.exit(1)
}


app.use('/api/doctors', authMiddleWare, doctorController);
app.use('/api/branch', authMiddleWare, branchController);
app.use('/api/users', controllers)
app.use('/api/auth', authController)



const port = process.env.port || 6095
app.listen(port, () => console.log(`Server is running on ${port}`))