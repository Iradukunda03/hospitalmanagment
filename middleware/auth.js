const config = require('config')
const jwt  =  require('jsonwebtoken')

module.exports = function (req,res,next){
    const token = req.header('x-auth-token')
    if(!token) return res.send('TOKEN MISSING..').status(401)
    try {
        const decoded = jwt.verify(token, config.get('jwt_privateKey'))
        req.user = decoded
        next()
    } catch (err) {
        return  res.send(" error occured").status(400)
    }
}